package com.example.actividad4androidbasico2_marinbrahyan.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.actividad4androidbasico2_marinbrahyan.Fragments.LoginFragment
import com.example.actividad4androidbasico2_marinbrahyan.R


class AccessFragment : Fragment(), View.OnClickListener {

    var btnLoginAccess:Button?=null
    var btnSingUpAccess:Button?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_access, container, false)

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)




        btnLoginAccess=view.findViewById(R.id.btnLoginAccess)
        btnSingUpAccess=view.findViewById(R.id.btnSingUpAccess)

        btnLoginAccess!!.setOnClickListener(this)
        btnSingUpAccess!!.setOnClickListener(this)


    }

    override fun onClick(v: View?) {

        /*
        Se declaran los fragmentos de Registro y Login para poder reemplazar al Fragment Access
        en el contenedor flContainer = Frame Layot
         */

        val LoginFragment:LoginFragment= LoginFragment()
        val SingUpFragment:SingUpFragment= SingUpFragment()

        if(v==btnLoginAccess){

            /*
            Se utiliza addToBackStack(null) para añadir a la pila el Fragment que se sustituye, para
            que al pulsar el botón volver desde culquiera de los dos fragmentos, se vuelva al fragmento
            de acceso, y no se cierre la apliación
             */

            fragmentManager?.beginTransaction()?.apply {
                replace(R.id.flContainer,LoginFragment)
                addToBackStack(null)
                commit()
            }

        }else if(v==btnSingUpAccess){

            fragmentManager?.beginTransaction()?.apply {
                replace(R.id.flContainer,SingUpFragment)
                addToBackStack(null)
                commit()
            }

        }
    }


}