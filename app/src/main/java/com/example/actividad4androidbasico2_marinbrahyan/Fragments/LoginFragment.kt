package com.example.actividad4androidbasico2_marinbrahyan.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.actividad4androidbasico2_marinbrahyan.R


class LoginFragment : Fragment(), View.OnClickListener {

    var btnAccederLogin:Button?=null
    var btnVolverLogin:Button?=null
    var etPasswordLogin:EditText?=null
    var etEmailLogin:EditText?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnAccederLogin=view.findViewById(R.id.btnAccederLogin)
        btnVolverLogin=view.findViewById(R.id.btnVolverLogin)
        etPasswordLogin=view.findViewById(R.id.etPasswordLogin)
        etEmailLogin=view.findViewById(R.id.etEmailLogin)

        btnAccederLogin!!.setOnClickListener(this)
        btnVolverLogin!!.setOnClickListener(this)


    }

    override fun onClick(v: View?) {

        /*
       Declaracion del primer fragmento para poder volver a el, en caso de pulsar el "boton Volver"
        */

        val AccessFragment:AccessFragment=AccessFragment()

        /*
        se alamcena en variables locales el valor de los textos que el usuario ha introducido, para
        que sea mas facil trabajar con ellos, y evaluar si funciona "'todo'" correctamente
         */

        var sEmail:String?=etEmailLogin?.text.toString()
        var sPassword:String?=etPasswordLogin?.text.toString()

        if(v==btnAccederLogin){

            /*
            Se usa la funcion TOAST para controlar si funciona el renonocimeinto de credenciales
            para controlar el acceso a la apliacion
             */

            if ((sEmail=="admin@admin.com" && sPassword=="Admin1234")||(sEmail=="user@user.com" && sPassword=="User1234")){

                Toast.makeText(activity, "Credenciales Correctas", Toast.LENGTH_SHORT).show()

            }else{

                Toast.makeText(activity, "Autenficacion Fallida", Toast.LENGTH_SHORT).show()
            }

        }else if(v==btnVolverLogin){

            /*
            De esta manera se pueden realizar transiciones entre fragmentos
             */

            fragmentManager?.beginTransaction()?.apply {
                replace(R.id.flContainer,AccessFragment)
                commit()
            }

        }

    }


}