package com.example.actividad4androidbasico2_marinbrahyan.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.actividad4androidbasico2_marinbrahyan.R


class SingUpFragment : Fragment(), View.OnClickListener {

    var etUserNameSingUp:EditText?=null
    var etEmailSingUp:EditText?=null
    var etPasswordSingUp:EditText?=null
    var etRepeatPasswordSingUp:EditText?=null
    var btnVolverSingUp:Button?=null
    var btnRegistrarmeSingUp:Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_sing_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etUserNameSingUp=view.findViewById(R.id.etUserNameSingUp)
        etEmailSingUp=view.findViewById(R.id.etEmailSingUp)
        etPasswordSingUp=view.findViewById(R.id.etPasswordSingUp)
        etRepeatPasswordSingUp=view.findViewById(R.id.etRepeatPasswordSingUp)
        btnVolverSingUp=view.findViewById(R.id.btnVolverSingUp)
        btnRegistrarmeSingUp=view.findViewById(R.id.btnRegistrarmeSingUp)

        btnVolverSingUp!!.setOnClickListener(this)
        btnRegistrarmeSingUp!!.setOnClickListener(this)
    }

    override fun onClick(k: View?) {

        /*
        Declaracion del primer fragmento para poder volver a el, en caso de pulsar el "boton Volver"
         */
        val AccessFragment:AccessFragment=AccessFragment()

        if(k==btnVolverSingUp){

            /*
            De esta manera se pueden realizar transiciones entre fragmentos
             */

            fragmentManager?.beginTransaction()?.apply {
                replace(R.id.flContainer,AccessFragment)
                commit()
            }

        }else if(k==btnRegistrarmeSingUp){

            /*
            Vincular el contenido de los diversos campos que debe rellenar el usuario para registrarse
            a varibales locales, con las que es mas facil trabajar
             */

            var strUsername:String=etUserNameSingUp!!.text.toString()
            var strEmail:String=etEmailSingUp!!.text.toString()
            var strPassword1:String=etPasswordSingUp!!.text.toString()
            var strPassword2:String=etRepeatPasswordSingUp!!.text.toString()

            /*
            Se evalua el cumplimiento de los diferentes requisitos para validar el registro, mediante
            llamadas sucesivas a funciones que evaluan la validez de los datos, en cada paso se controla
            que condicion se incumple mediante la impresion de un mesnaje durante la ejecucion usando la
            funcion TOAST
             */

            if((UsernameValidator(strUsername))==false){
                Toast.makeText(activity,"El nombre de usuario debe tener al menos 5 caracteres", Toast.LENGTH_LONG).show()
            }else if((EmailValidator(strEmail))==false) {
                Toast.makeText(activity, "El Email introducido no es valido", Toast.LENGTH_LONG).show()
            }else if(strPassword2!=strPassword1){
                Toast.makeText(activity,"Las contraseñas no coinciden", Toast.LENGTH_LONG).show()
            }else if((PasswordValidator(strPassword1))==false){
                Toast.makeText(activity,"Las contraseña debe contener al menos una mayuscula, una minuscula y un numero", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(activity,"Registro completado con exito, revise su bandeja de entrada para validar el Email", Toast.LENGTH_LONG).show()
            }


        }

    }

    /*
    Valida que las contraseña cumpla con los requisitos de tener al menos una mayuscula y una minuscula
    8 caracteres y al menos un numero
    */
     private fun PasswordValidator(Password:String):Boolean{

        var iContador:Int=0
        var bNumero:Boolean=false
        var bMayuscula:Boolean=false
        var bValida:Boolean=false
        var bMinuscula:Boolean=false

        for (i in Password){

            var caracter:String?=i.toString()

            if(caracter=="9"|| caracter=="8"|| caracter=="7"|| caracter=="6"|| caracter=="5"|| caracter=="4"|| caracter=="3"|| caracter=="2"|| caracter=="1"|| caracter=="0"){
                bNumero=true
            }

            if((caracter!=(caracter?.toLowerCase()))){
                bMayuscula=true
            }

            if((caracter!=(caracter?.toUpperCase()))){
                bMinuscula=true
            }

            iContador=iContador+1

        }

        if((bNumero==true)&&(bMayuscula==true)&&(bMinuscula==true)&&(iContador>=8)){

            bValida=true
        }

        return bValida
    }

    /*
     Valida que el Email sea valido, comprobando que el email tenga solo una "@" y que haya un punto
     despues de esta para comprobar que tiene dominio
     */
    private fun EmailValidator(Email:String):Boolean{

        var bValido:Boolean=false
        var bArroba:Boolean=false
        var bPunto:Boolean=false
        var iCuentaArrobas:Int=0

        //Evalua que despues de la arroba haya al menos un punto, para comprobar que el correo
        //Tenga un dominio, ademas previamente comprueba que haya una 'arroba'.
        //Además comprueba que solo haya una arroba, si hay más ta,bien dará por INVALIDO el Email

        for (i in Email){
            var caracter:String?=i.toString()
            if(caracter=="@"){
                bArroba=true
                iCuentaArrobas=iCuentaArrobas+1
            }

            if(iCuentaArrobas>1){
                bArroba=false
            }

            if((bArroba==true)&&(caracter==".")){
                bPunto=true
            }
        }
        if((bArroba==true)&&(bPunto==true)){
            bValido=true
        }

        return bValido
    }

     /*
      valida que el Username sea de al menos 5 caracteres
     */
    fun UsernameValidator(Username:String):Boolean{

        var iContador:Int=0

        for (i in Username){
            iContador=iContador+1
        }

        if(iContador>=5){
            return true
        }else{
            return false
        }
    }

}