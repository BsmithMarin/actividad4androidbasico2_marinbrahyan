package com.example.actividad4androidbasico2_marinbrahyan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.actividad4androidbasico2_marinbrahyan.Fragments.AccessFragment
import com.example.actividad4androidbasico2_marinbrahyan.Fragments.LoginFragment

class AccessActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_access)

        val AccessFragment:AccessFragment=AccessFragment()

        /*
        En esta actividad unicamenete se gestiona la aparicion del fragmento Access, a partir del cual
        se controlara el acceso a los otros dos fragmentos que forman parte de la actividad.
        para gestionar el cambio de Fragmentos se ha creado un contenedor de fragmentos, el flContainer
        */

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flContainer,AccessFragment)
            commit()
        }


    }


}



