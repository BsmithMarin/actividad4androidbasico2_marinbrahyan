package com.example.actividad4androidbasico2_marinbrahyan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var btnSalirInicio:Button?=null
    var btnAccederInicio:Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnAccederInicio=this.findViewById(R.id.btnAccederInicio)
        btnSalirInicio=this.findViewById(R.id.btnSalirInicio)

        btnSalirInicio!!.setOnClickListener(this)
        btnAccederInicio!!.setOnClickListener(this)

    }

    /*
    Control mediante consola del ciclo de vida de la activity
     */

    override fun onPause() {
        super.onPause()
        Log.v("Pausa","APLICACION EN SEGUNDO PLANO")
    }
    override fun onResume() {
        super.onResume()
        Log.v("Reanudar","APLICACION REANUDADA")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("Reanudar","APLICACION FINALIZADA")

    }

    override fun onClick(p0: View?) {
        if (p0==btnAccederInicio){

            //Cambio de activity, se finaliza al avitvidad actual acto seguido

            val intent: Intent = Intent(this, AccessActivity :: class.java)
            startActivity(intent)
            this.finish()

        }else if (p0==btnSalirInicio){

            //Fuerza la salida de la apliacion en caso de pulsar el boton

            this.onDestroy()
            this.finish()
            Log.v("Destroyed","Aplicacion finalizada")


        }
    }
}